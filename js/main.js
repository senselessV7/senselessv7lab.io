$(document).ready(function(){
    // █████████ PAGE FADE OUT █████████   

    $("body").fadeIn(2000);

    $("a.transition-x").click(function(event){
        event.preventDefault();
        linkLocation = this.href;
        $("body").fadeOut(4000, redirectPage);
        $("a.transition-x").animate({color: "#ff3333"},1000);
    });

    function redirectPage() {
        window.location = linkLocation;
    };
    // ██████--------------------███████
    // ███████████ VIDEO BG ████████████
    $("video").prop('muted', true);

      $("#mute-sound").click( function (){
        if( $("video").prop('muted') ) {
              $("video").prop('muted', false);
        } else {
          $("video").prop('muted', true);
        }
      });
    // ██████---------------------██████
    // ███████ BUTTON FOR MOBILE ███████
    $( ".arrow.mob" ).click(function( event ) {
        // event.preventDefault();
        event.stopPropagation();
        $( "#menu" ).slideToggle(1200);                
    });

    if (document.body.clientWidth <= '850') {
        $(document).click(function() {
            $('#menu').slideUp(900);       
        });
    };
    // ███████-------------------███████
    // ██████ SOUD FOR NAVIGATION ██████
    $("nav#menu a") // loop each menu item
      .each(function(i) {
        if (i != 0) { // only clone if more than one needed
          $("#sound")
            .clone()
            .attr("id", "sound-" + i)
            .appendTo($(this).parent()); 
        }
        $(this).data("sounder", i); // save reference 
      })
      .mouseenter(function() {
        $("#sound-" + $(this).data("sounder"))[0].play();
      });
    $("#sound").attr("id", "sound-0"); // get first one into naming convention
    // ███████-------------------███████
    // ██████  VISUAL NAVIGATION  ██████
    $('#menu').visualNav({
        // *** Menu ***
        // add a link class, as necessary (e.g. 'a.menu')
        link              : 'a',
        // added in case you have link = "div" and attribute something like
        targetAttr        : 'href',
        // where to add the selected class name; to only apply to the link,
        // use the same value as is in the link option ('a')
        selectedAppliedTo : 'li',
        // *** Content ***
        // content class to get height of the section
        contentClass      : 'content',
        // class name of links inside the content that act like the visualNav menu
        // (smooth scroll)
        contentLinks      : 'visualNav',
        // class name of links that link to external content.
        externalLinks     : 'external',
        // if true, the location hash will be updated
        useHash           : true,
        // *** Classes added to menu items ***
        // css class added to items in the viewport
        inViewClass       : 'inView',
        // css class applied to menu when a link is selected (highlighted)
        selectedClass     : 'selected',
        // *** Appearance ***
        // margin from the end of the page where the last menu item is used
        // (in case the target is short)
        bottomMargin      : 200,
        // If true, the contentClass width will be adjusted to fit the browser
        // window (for horizontal pages).
        fitContent        : false,
        // *** Animation ***
        // time in milliseconds
        animationTime     : 2000,
        // horizontal, vertical easing; it might be best to leave one axis as
        // swing [ 'swing', 'easeInCirc' ]
        easing            : [ 'easeOutCirc', 'easeOutCirc' ],
        // *** Callbacks ***
        // visNav is visualNavigation object (see the methods page)
        // $selected is a jQuery object of the currently selected content
        // (class name from "contentClass")
        // Callback executed before the animation begins moving to the targetted
        // element
        beforeAnimation   : function(visNav, $selected){ },
        // Callback executed when the targetted element is in view and scrolling
        // animation has completed
        complete          : function(visNav, $selected){ }
    });
    // ███████--------------------------███████
    // ██████  PARALLAX EFFECT LANGUAGE  ██████
  // ******
  //.parallax(xPosition, speedFactor, outerHeight) опции:
  //xPosition - горизонтальная позиция элемента
  //speedFactor - скорость прокрутки
  //outerHeight (true/false)  
  
  // $('#intro').parallax("50%", 2);
    $('#lang').parallax("50%", 0.1);
    $('#work').parallax("50%", 0.5);
    $('.bg-lang').parallax("50%", 0.4);
    $('#i_love').parallax("50%", 0.1); 
    $('#content-work').parallax("50%", 0.5);  
    // ███████--------------------███████
    // ██████  SKILLS KNOB PLUGIN  ██████
    $('.knob').knob({
        change : function (value) {
            //console.log("change : " + value);
        },
        release : function (value) {
            //console.log(this.$.attr('value'));
            console.log("release : " + value);
        },
        cancel : function () {
            console.log("cancel : ", this);
        },
        draw : function () {
            // "tron" case
            if(this.$.data('skin') == 'tron') {

                var a = this.angle(this.cv)  // Angle
                    , sa = this.startAngle          // Previous start angle
                    , sat = this.startAngle         // Start angle
                    , ea                            // Previous end angle
                    , eat = sat + a                 // End angle
                    , r = 1;
                this.g.lineWidth = this.lineWidth;
                this.o.cursor
                    && (sat = eat - 0.3)
                    && (eat = eat + 0.3);
                if (this.o.displayPrevious) {
                    ea = this.startAngle + this.angle(this.v);
                    this.o.cursor
                        && (sa = ea - 0.3)
                        && (ea = ea + 0.3);
                    this.g.beginPath();
                    this.g.strokeStyle = this.pColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                    this.g.stroke();
                }
                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                this.g.stroke();
                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();
                return false;
            }
        }
    });
    $('.knob').knob();
        $(window).scroll(function(){        
            if($(window).scrollTop()+$('header').height() > $('#education').position().top - $('header').height()){
  
                $('.knob').each(function () {              
                    var $this = $(this);
                    var myVal = $this.attr("rel");
                    // alert(myVal);
                    $this.knob({
                    });
                    $({  
                        value: 0                           
                    }).animate({
                        value: myVal
                    }, {                                       
                        duration: 3000,
                        easing: 'swing',
                        step: function () {
                            $this.val(Math.ceil(this.value)).trigger('change');
                        }
                    })
                });
                
        }        
    });
    // Example of infinite knob, iPod click wheel
    var v, up=0,down=0,i=0
        ,$idir = $("div.idir")
        ,$ival = $("div.ival")
        ,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
        ,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };

    $("input.infinite").knob({
        min : 0
        , max : 20
        , stopper : false
        , change : function () {
                        if(v > this.cv){
                            if(up){
                                decr();
                                up=0;
                            }else{up=1;down=0;}
                        } else {
                            if(v < this.cv){
                                if(down){
                                    incr();
                                    down=0;
                                }else{down=1;up=0;}
                            }
                        }
                        v = this.cv;
                    }
    });    
    // ███████--------------███████
    // ██████  BG SLIDESHOW  ██████
    $('#slideShow').bgStretcher({
        // All options:
        images: ['images/bg-slide-1.png', 'images/bg-slide-2.png', 'images/bg-slide-3.png', 'images/bg-slide-4.png', 'images/bg-slide-5.png'], //массив с именами изображений, которые будут использованы для слайда
        
        //imageContainer:             'bgstretcher', //имя div элемента в рамках которого работает слайдшоу фоновых изображений
        resizeProportionally:       true, //пропорциональное изменение размеров изображений
        resizeAnimate:              true, //изменение размеров анимации
        imageWidth:                 5200, //ширина исходного изображения в px
        imageHeight:                1500, //высота исходного изображения в px
        maxWidth:                   'auto', //максимальная ширина слайда, можно указать в px, но лучше auto-чтобы изображение растягивалось на все окно
        maxHeight:                  '750px', //максимальная высота слайда, можно указать в px, но лучше auto-чтобы изображение растягивалось на все окно
        nextSlideDelay:             8000, //задержка в миллисекундах, на каждом слайде
        slideShowSpeed:             8000, //скорость пролистывания слайда, можно задать в миллисекундах
        slideShow:                  true, //переключатель true-
        transitionEffect:            'simpleSlide', // Эффект перелистывания(none, fade, simpleSlide, superSlide)
        slideDirection:                'N', // Направления смены слайда(север-N, юг-S, запад-W, восток-E, (Если эффект superSlide - NW, NE, SW, SE)
        sequenceMode:                'normal', // Порядок перелистывания (normal-в порядке возрастания массива images, back-в порядке убывания массива images, в случайном порядке
        buttonPrev:                    '', //Указывается id или class, объекта html-структуры - кнопка назад
        buttonNext:                    '', //Указывается id или class, объекта html-структуры - кнопка вперед
        pagination:                 '', //Указывается id или class, объекта html-структуры - навигации по слайдам
        anchoring:                     'left top', // Позиция выравнивания - right bottom center
        anchoringImg:                 'left top', // Позиция выравнивания изображений- right bottom center
        preloadImg:                    false, //предварительная загрузка файлов(false-нет,true-да)
        startElementIndex:            0, //начинать отсчет пролистываемых элементов с индекса, указанного в данном параметре
        callbackfunction:            null // указывается функция javascript, которая будет выполнятся каждый раз, когда изменился слайд
        //Read more: http://www.websoldier.ru/kak-sdelat-slajder-fonovogo-izobrazheniya-smenyayushhijsya-fon-sajta-na-css-i-jquery.html#ixzz2miaciePm
    });
      
});
  
